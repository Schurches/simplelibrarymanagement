﻿using SimpleLibraryModule.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SimpleLibraryModule.Controllers
{
    public class AuthController : Controller
    {

        

        // GET: Auth
        public ActionResult Index()
        {
            return View();
        }

        // GET: Auth
        public ActionResult SignIn()
        {
            return View();
        }

        // GET: Auth/SignUp
        public ActionResult SignUp()
        {
            using(simpleLibraryEntities1 db = new simpleLibraryEntities1())
            {
                List<TipoIdentificacion> tiposDeUsuario = db.TipoIdentificacions.ToList<TipoIdentificacion>();
                ViewBag.userTypes = tiposDeUsuario;
                return View();
            }
            
        }

        [HttpPost]
        public ActionResult SignUp(Usuario user)
        {
            int usertypeID = Convert.ToInt32(Request.Form["tipoSelector"]);
            user.tipoid = usertypeID;
            user.password = ConvertStringtoMD5(Request.Form["password1"]);
            user.creación = System.DateTime.Now;
            user.estado = true;
            using (simpleLibraryEntities1 db = new simpleLibraryEntities1()) {
                db.Usuarios.Add(user);
                db.SaveChanges();
            }
            return RedirectToAction("SignIn");
        }

        // POST: Auth/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Auth/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Auth/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Auth/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Auth/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }




        public byte[] ConvertStringtoMD5(string word)
        {
            MD5 md5 = MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(word);
            byte[] hash = md5.ComputeHash(inputBytes);
            /*StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("x2"));
            }*/
            return hash;
        }

    }
}
