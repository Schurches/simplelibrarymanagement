//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SimpleLibraryModule.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Ejemplar
    {
        public int id { get; set; }
        public int libroid { get; set; }
        public int estadolibroid { get; set; }
        public bool estado { get; set; }
    
        public virtual Estado Estado1 { get; set; }
        public virtual Libro Libro { get; set; }
    }
}
