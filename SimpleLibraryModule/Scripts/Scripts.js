﻿function ajaxUserSubmit(form) {
    if (doPasswordsMatch()) {
        if ($(form).valid()) {
            var ajaxConfig = {
                type: 'POST',
                url: form.action,
                data: new FormData(form),
                success: function (response) {
                    if (response == success) {
                        top.location.href = "SignIn.html";
                    }
                }
            }
            if ($(form).attr('enctype') == "multipart/form-data") {
                ajaxConfig["contentType"] = false;
                ajaxConfig["processData"] = false;
            }
            $.ajax(ajaxConfig);
        }
    }
    return false;
}

function doPasswordsMatch() {

    var password = $('#password1');
    var confirm = $('#password2');
    var message = $('#confirm-message');
    if (password.val() == confirm.val()) {
        message.removeClass("alert-danger").addClass("alert-success").html("¡Las contraseñas concuerdan!");
        return true;
    } else {
        message.removeClass("alert-success").addClass("alert-danger").html("¡Las contraseñas no concuerdan!");
        return false;
    }
    
}