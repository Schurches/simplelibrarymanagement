USE [master]
GO
/****** Object:  Database [simpleLibrary]    Script Date: 8/06/2019 3:21:51 p. m. ******/
CREATE DATABASE [simpleLibrary]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'simpleLibrary', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\simpleLibrary.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'simpleLibrary_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\simpleLibrary_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [simpleLibrary] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [simpleLibrary].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [simpleLibrary] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [simpleLibrary] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [simpleLibrary] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [simpleLibrary] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [simpleLibrary] SET ARITHABORT OFF 
GO
ALTER DATABASE [simpleLibrary] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [simpleLibrary] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [simpleLibrary] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [simpleLibrary] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [simpleLibrary] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [simpleLibrary] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [simpleLibrary] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [simpleLibrary] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [simpleLibrary] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [simpleLibrary] SET  DISABLE_BROKER 
GO
ALTER DATABASE [simpleLibrary] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [simpleLibrary] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [simpleLibrary] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [simpleLibrary] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [simpleLibrary] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [simpleLibrary] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [simpleLibrary] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [simpleLibrary] SET RECOVERY FULL 
GO
ALTER DATABASE [simpleLibrary] SET  MULTI_USER 
GO
ALTER DATABASE [simpleLibrary] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [simpleLibrary] SET DB_CHAINING OFF 
GO
ALTER DATABASE [simpleLibrary] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [simpleLibrary] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [simpleLibrary] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'simpleLibrary', N'ON'
GO
ALTER DATABASE [simpleLibrary] SET QUERY_STORE = OFF
GO
USE [simpleLibrary]
GO
/****** Object:  Table [dbo].[Autor]    Script Date: 8/06/2019 3:21:51 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Autor](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[código] [varchar](50) NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[tipoid] [int] NOT NULL,
	[estado] [bit] NOT NULL,
 CONSTRAINT [PK_Autor] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Categoria]    Script Date: 8/06/2019 3:21:51 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categoria](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[descripcion] [varchar](200) NULL,
	[estado] [bit] NOT NULL,
 CONSTRAINT [PK_Categoria] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ejemplar]    Script Date: 8/06/2019 3:21:51 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ejemplar](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[libroid] [int] NOT NULL,
	[estadolibroid] [int] NOT NULL,
	[estado] [bit] NOT NULL,
 CONSTRAINT [PK_Ejemplar] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Estado]    Script Date: 8/06/2019 3:21:51 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Estado](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[estado] [bit] NOT NULL,
 CONSTRAINT [PK_Estado] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Libros]    Script Date: 8/06/2019 3:21:51 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Libros](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[título] [varchar](50) NOT NULL,
	[autor] [int] NOT NULL,
	[ingreso] [date] NOT NULL,
	[categoría] [int] NOT NULL,
	[estado] [bit] NOT NULL,
 CONSTRAINT [PK_Libros] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Prestamo]    Script Date: 8/06/2019 3:21:51 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Prestamo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[libroid] [int] NOT NULL,
	[usuarioid] [int] NOT NULL,
	[fechaPrestamo] [datetime] NOT NULL,
	[fechaDevolucion] [datetime] NOT NULL,
	[estado] [bit] NOT NULL,
 CONSTRAINT [PK_Prestamo] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TipoIdentificacion]    Script Date: 8/06/2019 3:21:51 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoIdentificacion](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[estado] [bit] NOT NULL,
 CONSTRAINT [PK_TipoIdentificacion] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Usuarios]    Script Date: 8/06/2019 3:21:51 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuarios](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[tipoid] [int] NOT NULL,
	[codigo] [varchar](10) NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[password] [varbinary](50) NOT NULL,
	[creación] [date] NOT NULL,
	[estado] [bit] NOT NULL,
 CONSTRAINT [PK_Usuarios] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[TipoIdentificacion] ON 

INSERT [dbo].[TipoIdentificacion] ([id], [nombre], [estado]) VALUES (1, N'Estudiante', 1)
INSERT [dbo].[TipoIdentificacion] ([id], [nombre], [estado]) VALUES (2, N'Profesor', 1)
INSERT [dbo].[TipoIdentificacion] ([id], [nombre], [estado]) VALUES (3, N'Funcionario', 1)
SET IDENTITY_INSERT [dbo].[TipoIdentificacion] OFF
SET IDENTITY_INSERT [dbo].[Usuarios] ON 

INSERT [dbo].[Usuarios] ([id], [tipoid], [codigo], [nombre], [password], [creación], [estado]) VALUES (2, 1, N'200074667', N'giordy', 0xA67995AD3EC084CB38D32725FD73D9A3, CAST(N'2019-06-03' AS Date), 1)
SET IDENTITY_INSERT [dbo].[Usuarios] OFF
ALTER TABLE [dbo].[Autor]  WITH CHECK ADD  CONSTRAINT [FK_Autor_TipoIdentificacion] FOREIGN KEY([tipoid])
REFERENCES [dbo].[TipoIdentificacion] ([id])
GO
ALTER TABLE [dbo].[Autor] CHECK CONSTRAINT [FK_Autor_TipoIdentificacion]
GO
ALTER TABLE [dbo].[Ejemplar]  WITH CHECK ADD  CONSTRAINT [FK_Ejemplar_Estado] FOREIGN KEY([estadolibroid])
REFERENCES [dbo].[Estado] ([id])
GO
ALTER TABLE [dbo].[Ejemplar] CHECK CONSTRAINT [FK_Ejemplar_Estado]
GO
ALTER TABLE [dbo].[Ejemplar]  WITH CHECK ADD  CONSTRAINT [FK_Ejemplar_Libros] FOREIGN KEY([libroid])
REFERENCES [dbo].[Libros] ([id])
GO
ALTER TABLE [dbo].[Ejemplar] CHECK CONSTRAINT [FK_Ejemplar_Libros]
GO
ALTER TABLE [dbo].[Libros]  WITH CHECK ADD  CONSTRAINT [FK_Libros_Autor] FOREIGN KEY([autor])
REFERENCES [dbo].[Autor] ([id])
GO
ALTER TABLE [dbo].[Libros] CHECK CONSTRAINT [FK_Libros_Autor]
GO
ALTER TABLE [dbo].[Libros]  WITH CHECK ADD  CONSTRAINT [FK_Libros_Categoria] FOREIGN KEY([categoría])
REFERENCES [dbo].[Categoria] ([id])
GO
ALTER TABLE [dbo].[Libros] CHECK CONSTRAINT [FK_Libros_Categoria]
GO
ALTER TABLE [dbo].[Prestamo]  WITH CHECK ADD  CONSTRAINT [FK_Prestamo_Libros] FOREIGN KEY([libroid])
REFERENCES [dbo].[Libros] ([id])
GO
ALTER TABLE [dbo].[Prestamo] CHECK CONSTRAINT [FK_Prestamo_Libros]
GO
ALTER TABLE [dbo].[Prestamo]  WITH CHECK ADD  CONSTRAINT [FK_Prestamo_Usuarios] FOREIGN KEY([usuarioid])
REFERENCES [dbo].[Usuarios] ([id])
GO
ALTER TABLE [dbo].[Prestamo] CHECK CONSTRAINT [FK_Prestamo_Usuarios]
GO
ALTER TABLE [dbo].[Usuarios]  WITH CHECK ADD  CONSTRAINT [FK_Usuarios_TipoIdentificacion] FOREIGN KEY([tipoid])
REFERENCES [dbo].[TipoIdentificacion] ([id])
GO
ALTER TABLE [dbo].[Usuarios] CHECK CONSTRAINT [FK_Usuarios_TipoIdentificacion]
GO
USE [master]
GO
ALTER DATABASE [simpleLibrary] SET  READ_WRITE 
GO
